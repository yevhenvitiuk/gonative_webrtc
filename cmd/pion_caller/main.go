package main

import (
	"fmt"
	"github.com/getlantern/systray"
	"github.com/getlantern/systray/example/icon"
	_ "github.com/pion/webrtc/v3"
	"io/ioutil"
	"log"
	"time"
)

var currentUser *User = &User{userStatusOnline}
var controller *trayController = NewTrayController(currentUser, NewCallManager(struct{}{}))

func main() {
	onExit := func() {
		now := time.Now()
		ioutil.WriteFile(fmt.Sprintf(`on_exit_%d.txt`, now.UnixNano()), []byte(now.String()), 0644)
	}

	systray.Run(onReady, onExit)
}

func onReady() {
	systray.SetTemplateIcon(icon.Data, icon.Data)
	systray.SetTitle("Pion caller")
	systray.SetTooltip("Pion caller app")

	// We can manipulate the systray in other goroutines
	go func() {
		//	systray.SetTemplateIcon(icon.Data, icon.Data)
		//	systray.SetTitle("Awesome App")
		//	systray.SetTooltip("Pretty awesome棒棒嗒")
		mStatus := systray.AddMenuItemCheckbox(controller.getStatusLabel(), controller.getStatusLabelTooltip(), currentUser.IsOnline())

		mRooms := systray.AddMenuItem("Rooms", "Manage rooms")
		submRoomCreate := mRooms.AddSubMenuItem("Create", "Create room")
		submRoomConnect := mRooms.AddSubMenuItem("Connect", "Connect to another room")

		systray.AddSeparator()

		mQuit := systray.AddMenuItem("Quit", "Quit the whole app")
		// Sets the icon of a menu item. Only available on Mac.
		mQuit.SetIcon(icon.Data)

		controller.SetMenus(mStatus, mRooms)
		for {
			select {
			case <-mStatus.ClickedCh:
				controller.OnStatusClicked(mStatus)
			case <-submRoomCreate.ClickedCh:
				controller.OnRoomCreateClicked(submRoomCreate)
			case <-submRoomConnect.ClickedCh:
				controller.OnRoomConnectClicked(submRoomConnect)
			case <-mQuit.ClickedCh:
				log.Println("Requesting quit")
				systray.Quit()
				log.Println("Finished quitting")
			}
		}
	}()
}
