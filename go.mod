module bitbucket.org/yevhenvitiuk/gonative_webrtc

go 1.16

require (
	github.com/getlantern/systray v1.1.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/pion/webrtc/v3 v3.0.19
)
