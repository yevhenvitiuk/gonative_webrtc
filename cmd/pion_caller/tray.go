package main

import (
	"bitbucket.org/yevhenvitiuk/gonative_webrtc/internal/pkg/webrtc"
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/getlantern/systray"
	"github.com/gofrs/uuid"
	webrtc2 "github.com/pion/webrtc/v3"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

/*
	TODO:
		1. Connection fails on callee side after caller has established connection ok.
		2. Simplify connection establishment
*/

type userStatus int

const (
	userStatusOnline userStatus = iota
	userStatusOffline
)

func (us userStatus) GoString() string {
	switch us {
	case userStatusOnline:
		return "Online"
	case userStatusOffline:
		return "Offline"
	}

	panic(fmt.Errorf("Unknown status: %v", us))
}

var (
	roomsCounter *rand.Rand = rand.New(rand.NewSource(time.Now().Unix()))
)

//type AppUser interface {
//	Status() userStatus
//}

type User struct {
	status userStatus
}

func (u *User) Status() userStatus {
	return u.status
}

func (u *User) SetStatus(newStatus userStatus) {
	u.status = newStatus
}

func (u *User) IsOnline() bool {
	return u.status == userStatusOnline
}

type signalMessage struct {
}

type SignalingServer interface {
	//Send(description webrtc2.SessionDescription) error
	//OnMessage(chan<- signalMessage) error
}

type RtcCallManager interface {
	CreateRoom(*User, string) (*CallRoom, error)
	ConnectToRoom(string) (interface{}, error)
	SubscribeToCallNotifications(*User) error
}

type CallManager struct {
	signallingServer SignalingServer
}

func (m *CallManager) send(payload interface{}, file string) error {
	var err error

	f, err := os.Create(file)
	if err != nil {
		log.Println("")
		return err
	}
	defer f.Close()
	return gob.NewEncoder(f).Encode(payload)

	data, err := json.Marshal(payload)
	if err == nil {
		to := base64.StdEncoding.EncodeToString(data)
		log.Println("send: data: \n", to)
		err = ioutil.WriteFile(file, []byte(to), 0644)
	}

	if err != nil {
		log.Println("send: Marshal/EncodeToString/WriteFile error: ", err)
		return err
	}
	return nil
}

func (m *CallManager) CreateRoom(user *User, name string) (*CallRoom, error) {
	//TODO: Add server network call
	//data, connInfo, err := m.signallingServer.
	//go func() {
	//TODO: Wait for reply message with sessionDescription{type:SDPTypeAnswer} from callee
	//TODO: how to exit from this goroutine on middle-error?
	//	messageCh := make(chan signalMessage)
	//	m.signallingServer.OnMessage(messageCh)
	//}()
	pc, err := m.initPeerConnection(nil)
	if err != nil {
		return nil, err
	}
	log.Println(pc.LocalDescription() == nil, pc.RemoteDescription() == nil)
	{
		//m.signallingServer.Send(pc.LocalDescription())
		ldesc := pc.LocalDescription()
		if err := m.send(ldesc, "conn.data"); err != nil {
			log.Println("CreateRoom: send error: ", err)
			return nil, err
		}

		puller := time.NewTicker(time.Second * 3)
		for {
			<-puller.C
			var rdesc webrtc2.SessionDescription
			if err = m.receive(&rdesc, "rconn.data"); err == nil {
				err = pc.SetRemoteDescription(rdesc)
			}
			if err != nil {
				log.Println("rconn.data receive error", err, ".Retry in 3s...")
				continue
			}

			puller.Stop()
			break
		}
		time.Sleep(time.Second * 2)
		log.Println("CreateRoom: reading candidates.")
		n, err := m.readCandidates(pc, "candidate_callee_")
		if err != nil {
			return nil, err
		}
		log.Println("!!!!!CreateRoom: read candidates.", n)
		if err := m.send(struct {
			String string
		}{"unlocked"}, "remote.unlock"); err != nil {
			return nil, err
		}
	}
	log.Println("!!!!")
	time.Sleep(time.Second * 15)
	log.Println("STARTING....")
	log.Println("CreateRoom local:\n", pc.LocalDescription(), "\nremote:\n", pc.RemoteDescription())

	channel, err := pc.CreateDataChannel("audio-channel", nil)
	if err != nil {
		return nil, err
	}
	{
		waitCh := make(chan struct{})

		channel.OnOpen(func() {
			log.Println("1")
			waitCh <- struct{}{}
		})
		select {
		case <-waitCh:
		}
		log.Println("2")
	}

	pc.OnDataChannel(func(channel *webrtc2.DataChannel) {
		log.Println("->Caller: Got channel: ", channel.ReadyState())
		channel.OnMessage(func(msg webrtc2.DataChannelMessage) {
			log.Println("->Caller: Channel: Got message: ", string(msg.Data))
			if msg.IsString && strings.IndexAny(string(msg.Data), "EXITING") != -1 {
				err := channel.SendText("WAIT, READ THIS MESSAGE: no audio implemented. You can't.")
				log.Println("Respond ", err == nil, err)
			}
		})
	})

	channel.OnMessage(func(msg webrtc2.DataChannelMessage) {
		log.Println("->Got message: ", string(msg.Data))
		if msg.IsString && strings.IndexAny(string(msg.Data), "EXITING") != -1 {
			err := channel.SendText("WAIT, READ THIS MESSAGE: no audio implemented. You can't.")
			log.Println("Respond ", err == nil, err)
		}
	})
	err = channel.SendText(fmt.Sprintf("GOOD WORLD. EXITING."))
	log.Println("Respond ", err == nil, err)

	id, _ := uuid.NewV4()

	return &CallRoom{id.String(), name, user, []*User{user}, pc}, nil
}

func (m *CallManager) initPeerConnection(desc *webrtc2.SessionDescription) (*webrtc2.PeerConnection, error) {
	pc, err := webrtc.NewPeerConnection(desc)
	if err != nil {
		return nil, err
	}

	var candidateIndex int
	var suf string = "caller"
	if desc != nil {
		suf = "callee"
	}

	pc.OnICECandidate(func(candidate *webrtc2.ICECandidate) {
		candidateIndex++
		if candidate == nil {
			log.Println(candidateIndex, "Candidate empty. Skipping...")
			return
		}
		log.Println(candidateIndex, "Got candidate: ", candidate.String())
		if err := m.send(candidate, fmt.Sprintf("candidate_%s_%d.data", suf, candidateIndex)); err != nil {
			log.Println("OnICECandidate: send error: ", err)
		}
	})

	pc.OnNegotiationNeeded(func() {
	})

	if desc == nil { //(1)CreateRoom first call
		if err := webrtc.CreateOffer(pc); err != nil {
			return nil, err
		}
	} else if desc.Type == webrtc2.SDPTypeOffer { //(3)Callee received offer after (1)
		err = pc.SetRemoteDescription(*desc)
		if err == nil {
			err = webrtc.CreateAnswer(pc) //(4)Callee respond to caller with his session info
		}
		err = m.send(pc.LocalDescription(), "rconn.data")
		//m.signallingServer.Send(pc.LocalDescription())
	} else if desc.Type == webrtc2.SDPTypeAnswer { //TODO: unreachable
		err = pc.SetRemoteDescription(*desc) //(5)Caller receives answer from callee
	}
	if err != nil {
		return nil, err
	}
	return pc, nil
}

func (m *CallManager) ConnectToRoom(roomName string) (interface{}, error) {
	//TODO: add server endpoint for room discovery
	//TODO: manage list of available rooms locally
	//data, err := m.signallingServer.
	//TODO: prevent connection to own room
	var desc webrtc2.SessionDescription
	if err := m.receive(&desc, "conn.data"); err != nil {
		return nil, err
	}

	pc, err := m.initPeerConnection(&desc)
	if err != nil {
		return nil, err
	}
	log.Println(pc.LocalDescription() == nil, pc.RemoteDescription() == nil)
	{
		log.Println("ConnectToRoom: reading candidates.")
		n, err := m.readCandidates(pc, "candidate_caller_")
		if err != nil {
			return nil, err
		}
		log.Println("ConnectToRoom: read candidates.", n)
		log.Println("Waiting for remote connected ok...")
		puller := time.NewTicker(time.Second * 3)
		for {
			<-puller.C
			var data struct {
				String string
			}
			if err = m.receive(&data, "remote.unlock"); err == nil && data.String == "unlocked" {
				log.Println("!!!!!Calle setup. Waiting for next message exchange")
			}
			if err != nil {
				log.Println("rconn.data receive error", err, ".Retry in 3s...")
				continue
			}

			puller.Stop()
			break
		}
	}
	log.Println("!!!!")
	time.Sleep(time.Second * 15)
	log.Println("STARTING....")
	log.Println("ConnectToRoom local:\n", pc.LocalDescription(), "\nremote:\n", pc.RemoteDescription())

	var audioChannel *webrtc2.DataChannel
	{
		channelCh := make(chan interface{})
		pc.OnDataChannel(func(channel *webrtc2.DataChannel) {
			log.Println("Waiting for channel opened")
			channel.OnOpen(func() {
				channelCh <- channel
			})
			channel.OnClose(func() {
				channelCh <- nil
			})
			channel.OnError(func(err error) {
				channelCh <- err
			})
		})
		go func() {
			time.Sleep(10 * time.Second)
			channelCh <- fmt.Errorf("ConnectToRoom timeout")
		}()

		if val := <-channelCh; val != nil {
			if err, ok := val.(error); ok {
				return nil, err
			}
			audioChannel = val.(*webrtc2.DataChannel)
		} else {
			return nil, fmt.Errorf("ConnectToRoom: unexpected channel close")
		}
		close(channelCh)
	}
	audioChannel.OnMessage(func(msg webrtc2.DataChannelMessage) {
		log.Println("<-Got message: ", string(msg.Data))
		err = audioChannel.SendText(fmt.Sprintf("GOOD WORLD. EXITING."))
		log.Println("Sent ", err)
	})

	return struct{}{}, nil
}

func (m *CallManager) readCandidates(pc *webrtc2.PeerConnection, matchPrefix string) (int, error) {
	dir, err := os.Open("./")
	if err != nil {
		return 0, err
	}
	defer dir.Close()
	dirEntries, err := dir.ReadDir(-1)
	if err != nil {
		return 0, err
	}
	var n int
	for _, f := range dirEntries {
		if !f.IsDir() && strings.Index(f.Name(), matchPrefix) == 0 {
			var candidate webrtc2.ICECandidate
			if err = m.receive(&candidate, fmt.Sprintf("./%s", f.Name())); err == nil {
				err = pc.AddICECandidate(candidate.ToJSON())
				if err == nil {
					n++
				}
			}
			if err != nil {
				log.Println("ConnectToRoom: receive: failed to add ICECandidate. file", f.Name(), err)
				continue
			}
		}
	}
	return n, nil
}

func init() {
	gob.Register(webrtc2.SessionDescription{})
	gob.Register(webrtc2.ICECandidate{})
}

func (m *CallManager) receive(obj interface{}, file string) error {
	var connInfo []byte
	connData, err := ioutil.ReadFile(file)
	if err != nil {
		log.Println("No file. Stopping...")
		return err
	}
	return gob.NewDecoder(bytes.NewBuffer(connData)).Decode(obj)

	connInfo, err = base64.StdEncoding.DecodeString(string(connData))
	if err != nil {
		log.Println("receive: base64.Decode error...", err)
		return err
	}
	log.Printf("receive: DecodeString '%v'\n", string(connInfo))
	if err := json.Unmarshal(connInfo, &obj); err != nil {
		log.Println("receive: Unmarshal error...", err)
		return err
	}
	return nil
}

func (m *CallManager) SubscribeToCallNotifications(user *User) error {
	return nil
}

type CallRoom struct {
	id           string
	name         string
	owner        *User
	participants []*User

	pc *webrtc2.PeerConnection //TODO: removeme
}

func NewCallManager(ssClient SignalingServer) RtcCallManager {
	return &CallManager{signallingServer: ssClient}
}

type trayController struct {
	user *User

	mStatus *systray.MenuItem
	mRooms  *systray.MenuItem

	callManager RtcCallManager
}

func (tc *trayController) SetMenus(mStatus, mRooms *systray.MenuItem) {
	tc.mStatus = mStatus
	tc.mRooms = mRooms
}

func (tc *trayController) Init() error {
	//TODO: Connect to signalling server in order to process incoming calls from signalling server
	if err := tc.callManager.SubscribeToCallNotifications(tc.user); err != nil {
		//TODO: Retry in another goroutine
		return err
	}
	return nil
}

func (tc *trayController) OnStatusClicked(mStatus *systray.MenuItem) {
	if tc.user.IsOnline() {
		tc.user.SetStatus(userStatusOffline)
		tc.mStatus.SetTitle(tc.getStatusLabel())
		tc.mRooms.Disable()
	} else {
		currentUser.SetStatus(userStatusOnline)
		tc.mStatus.SetTitle(tc.getStatusLabel())
		tc.mRooms.Enable()
	}
}

func (tc *trayController) OnRoomCreateClicked(submRoomCreate *systray.MenuItem) {
	room, err := tc.callManager.CreateRoom(tc.user, "Room_"+time.Now().Format(time.RFC822))
	if err != nil {
		log.Println("CreateRoom error: ", err)
		return
	}

	log.Printf("Room created: %s; \nn%v \n", room.name, room)
	log.Print("Waiting for connection...")
}

func (tc *trayController) OnRoomConnectClicked(submRoomConnect *systray.MenuItem) {
	//TODO: Show browser ui with rooms list or get current incoming room or ask to enter room to connect;
	var roomName = fmt.Sprintf("Room_" + time.Now().Format(time.RFC822))
	_, err := tc.callManager.ConnectToRoom(roomName)
	if err != nil {
		log.Println("ConnectToRoom error: ", err)
		return
	}

	log.Println("Connected to room ", roomName)
	log.Println("Starting call...")
}

func (tc *trayController) getStatusLabel() string {
	return fmt.Sprintf("Status: %#v", tc.user.Status())
}

func (tc *trayController) getStatusLabelTooltip() string {
	return "Click to toggle"
}

func NewTrayController(u *User, m RtcCallManager) *trayController {
	return &trayController{
		user:        u,
		callManager: m,
	}
}
