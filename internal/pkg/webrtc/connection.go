package webrtc

import (
	"github.com/pion/webrtc/v3"
	"github.com/pion/webrtc/v3/pkg/media/oggwriter"
	"log"
)

func NewPeerConnection(desc *webrtc.SessionDescription) (*webrtc.PeerConnection, error) {
	pc, err := webrtc.NewPeerConnection(webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	})
	if err != nil {
		return nil, err
	}

	pc.OnICEConnectionStateChange(func(connectionState webrtc.ICEConnectionState) {
		log.Printf("Connection State has changed %s \n", connectionState.String())
	})

	oggFile, err := oggwriter.New("output.ogg", 48000, 2)
	if err != nil {
		return nil, err
	}

	pc.OnTrack(func(track *webrtc.TrackRemote, receiver *webrtc.RTPReceiver) {
		log.Println("Got Opus track, saving to disk as output.ogg")
		log.Println("----->OnTrack: ", track.PayloadType())
		for {
			rtpPacket, _, readErr := track.ReadRTP()
			if readErr != nil {
				log.Println("--->OnTrack: ReadRTP error: ", readErr)
				break
			}
			if readErr := oggFile.WriteRTP(rtpPacket); readErr != nil {
				log.Println("--->OnTrack: WriteRTP error: ", readErr)
				break
			}
		}
	})

	if _, err = pc.AddTransceiverFromKind(webrtc.RTPCodecTypeAudio); err != nil {
		return nil, err
	}

	return pc, nil
}

func CreateAnswer(pc *webrtc.PeerConnection) error {
	answer, err := pc.CreateAnswer(nil)
	if err != nil {
		return err
	}

	return pc.SetLocalDescription(answer)
}

func CreateOffer(pc *webrtc.PeerConnection) error {
	offer, err := pc.CreateOffer(nil)
	if err != nil {
		return err
	}

	return pc.SetLocalDescription(offer)
}
